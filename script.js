
let cnv,
	ctx;

let ship,
	shipX,
	shipY,
	shipStepX = 16, // шаг хода корабля игрока
	shipStepY = 3,
	shipBuls = [],
	shipBullImg;

let enemyImg,
	enemies = [],
	enemyDestroyedCount = 0,
	enemyMissedCount = 0;

let youmovex = 0,
	youmovey = 0;

let score = 0, 
	endGame = 0, 
	gameTimer,
	gameTime = 30, // максимальная длительность игры
	gameTimeSpent = 0; // кол-во отыгранных секунд


// проверка столконовения вражеского корабля с пулями игрока 
// i - индекус вражеского корабля
// j - индекс пули игрока
function check_fire_enemy_bullet(i,j)
{
	let enemy_x = enemies[i].x, 
		enemy_y = enemies[i].y,
		bul_x = shipBuls[j].x,
		bul_y = shipBuls[j].y;

	if ( ( bul_x + shipBullImg.width >= enemy_x ) && ( bul_x <= enemy_x + enemyImg.width ) && ( bul_y + shipBullImg.height >= enemy_y) && ( bul_y <= enemy_y + enemyImg.height ) )
		return 1;

	return 0;
}


// добавление пуль игрока в массив и задание им начальных координат
function create_bullet()
{
	let tmp = {};
	tmp.x = shipX;
	tmp.y = shipY - 20;	
	shipBuls.push(tmp);
}

// добавление вражеских кораблей в массив и задание им начальных координат (х - произвольное число)
function create_enemies()
{
	if ( endGame == 1 )
		return;

	let tmp = {};	
	tmp.x = getRandomInt(0, cnv.width - enemyImg.width - 20);
	tmp.y = -enemyImg.height;
	enemies.push(tmp);
}

// задание новых координат пулям игрока (т.е. перемещение пуль) и удаление пуль и кораблей при их соприкосновении
function move_bullets()
{	
	let bul_del = 0;
	for (let i = shipBuls.length-1; i >= 0; i--) {
		bul_del = 0;
		shipBuls[i].y -= 7;
		
		for (let j = enemies.length-1; j >= 0; j--) {
			if ( bul_del == 0 )
				if ( check_fire_enemy_bullet(j,i) == 1 ) { // проверка столконовения вражеского корабля с пулями игрока
					enemies.splice(j,1);	
					shipBuls.splice(i,1);
					score += 10;
					enemyDestroyedCount++;
					bul_del = 1;
				}
		}		
		
		if (bul_del == 0)
			// если пуля ушла за пределы конваса, то удаляем её из массива пуль игрока
			if (shipBuls[i].y < -shipBullImg.height) {
				shipBuls.splice(i,1); 
			}
	}
}

// задание новых кородинат вражеским кораблям и проверка на их столкновения с кораблем игрока
function move_enemies()
{
	for (let i = 0; i < enemies.length; i++)
	{
		enemies[i].y += 4;

		if ( enemies[i].y >= cnv.height ) {
			enemies.splice(i,1);
			enemyMissedCount++;
			if ( enemyMissedCount  == 3 ) {
				endGame = 1;
				endGameResult();
				return 1;
			}
		} else if ( enemies[i].y + enemyImg.height >= shipY && enemies[i].x <= shipX + ship.width && enemies[i].x + enemyImg.width >= shipX ) {
			endGame = 1;
			endGameResult();
			return 1;
		}			
	}
}

function endGameResult() {
	clearInterval(gameTimer);
	$('.game-block').append(`<div class="game__popup  game__result-block">
		`+ lang_txt[siteLang]["score_txt"]+`  `+score+` 
		<div class="game__btn game__btn_restart" onclick="restartGame();">`+ lang_txt[siteLang]["restart_txt"]+`</div>
		</div>`);
}

function keyboard_keydown(e)
{
	if ( endGame == 1 )
		return;

	e.preventDefault();
	
	// пули корабля
	if (e.keyCode==32) { // пробел
		create_bullet();
	}

	// вычисляем на сколько сдвигать корабль при управлении стрелками
	if ( e.keyCode == 37 ) { // влево
		youmovex = -1;
		youmovey = 0;
	}
	if ( e.keyCode == 39 ) { // вправо
		youmovex = 1;
		youmovey = 0;
	}
	if (e.keyCode==38) { // наверх
		youmovex = 0;
		youmovey = -1;
	}
	if (e.keyCode==40) { // вниз
		youmovex = 0;
		youmovey = 1;
	}
}

function keyboard_keyup(e) {
	// прекращаем вычисление движения корабля
	youmovex = 0;
	youmovey = 0;
}

// меняет позицию корбаля
function moveShip() {
	if (shipX < 0)
		shipX = 0;
	else if (shipX > cnv.width - ship.naturalWidth)
		shipX = cnv.width - ship.naturalWidth;
	else
		shipX += youmovex*shipStepX;

	if (shipY < 0)
		shipY = 0;
	else if (shipY > cnv.height - ship.height - 20)
		shipY = cnv.height - ship.height - 20;
	else
		shipY += youmovey*shipStepY;
}

function timer()
{
	if (endGame == 1)
		return;

	move_bullets();

	move_enemies();

	document.onkeydown = keyboard_keydown;
	document.onkeyup = keyboard_keyup;
	moveShip();

	draw();
	window.setTimeout("timer();", 20);
}

function draw() {

	ctx.clearRect(0, 0, cnv.width, cnv.height);

	ctx.fillStyle = 'white';
	ctx.fillRect(0, 0, cnv.width, cnv.height);

	// отрисовка корабля
	ctx.drawImage(ship, shipX, shipY);
	
	// отрисовка пуль
	for (var i = 0; i < shipBuls.length; i++)
	{		
		ctx.drawImage(shipBullImg,shipBuls[i].x,shipBuls[i].y);
	}
	
	// отрисовка вражеских кораблей
	for (var i=0; i < enemies.length; i++)
	{
		ctx.drawImage(enemyImg, enemies[i].x, enemies[i].y);
	}

	// отрисовка набранных очков
	$('.game-res-destroyed-symptoms').text(enemyDestroyedCount);
	$('.game-res-score').text(score);
	$('.game-res-time').text(getRemainingTime);
	$('.game-res-miss-symptoms').text(enemyMissedCount);
}

function getRemainingTime(){
	tmp = Number(gameTime - gameTimeSpent);
    var h = Math.floor(tmp / 3600);
    var m = Math.floor(tmp % 3600 / 60);
    var s = Math.floor(tmp % 3600 % 60);

    var hDisplay = h > 0 ? h + ':' : "";
    var mDisplay = m > 0 ? m + ':' : "00:";
    var sDisplay = s > 0 ? s : "00";
    return hDisplay + mDisplay + sDisplay; 
}

function initGame() {
	$('.game-block__mobile-controls').addClass('active');  

	insertGameTxtBlock();

	cnv = document.getElementById("game");
	ctx = cnv.getContext("2d"); //возвращает контекст рисования на 

	// инициализация корабля
	ship = new Image();
	ship.src = 'ship.png';

	shipX = cnv.width / 2 - ship.width / 2;
	shipY = cnv.height - ship.height - 20;


	// инициализация картинки пуль корабля
	shipBullImg = new Image();
	shipBullImg.src = 'bullets.png';

	// инициализация картинки врагов
    enemyImg = new Image();
    enemyImg.src = 'enemy-mikrob.png';

	setInterval(create_enemies, 1400);
	gameTimer = setInterval(checkGameTime, 1000);
}

function restartGame() {
	insertGameTxtBlock();

	if ( $('.game__result-block').length )
		$('.game__result-block').remove();

	shipBuls = [];
	fameTime = 300;
	enemyDestroyedCount = 0;
	enemyMissedCount = 0;
	enemies = [];
	score = 0;
	endGame = 0;
	gameTimeSpent = 0;

	timer();
	gameTimer = setInterval(checkGameTime, 1000);
}

function insertGameTxtBlock() {
	if ( $('.game__text-block').length )
		$('.game__text-block').remove();

	$('.game-block').append(`<div class="game__text-block">
		<div class="game__text-row"> `+ lang_txt[siteLang]["score_txt"]+` <b class="game-res-score">0</b></div>
		<div class="game__text-row"> `+ lang_txt[siteLang]["enemy_txt"]+` <b class="game-res-destroyed-symptoms">0</b></div>
		<div class="game__text-row"> `+ lang_txt[siteLang]["miss_txt"]+` <b class="game-res-miss-symptoms">03:00</b></div>
		<div class="game__text-row"> `+ lang_txt[siteLang]["time_txt"]+` <b class="game-res-time color-black">03:00</b></div>
	</div>`);
}


function checkGameTime() {
	gameTimeSpent++;
	if ( gameTimeSpent >= gameTime ) {
		endGameResult();
		endGame = 1;
	}
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
}


function initMobileControls() {
	if ( !$('.game-block__mobile-controls').length )
		return;

	// пули корабля
	$(document).on('click', '.mobile-control_shoot', function() {
		create_bullet();
	})
	$(document).on('click', '.mobile-control_left', function() {
		youmovex = -1;
		youmovey = 0;
	})
	$(document).on('click', '.mobile-control_right', function() {
		youmovex = 1;
		youmovey = 0;
	})
}





window.addEventListener("load",function(){
	siteLang = getSiteLang();
	initMobileControls();
});


let siteLang;
let lang_txt = {
	ru : {
		"score_txt" : "Набрано очков:",
		"enemy_txt" : "Уничтожено симптомов:",
		"miss_txt" : "Пропущено:",
		"time_txt" : "Оставшееся время:",
		"restart_txt" : "Попробуй еще раз"
	},
	kz : {
		"score_txt" : "Жиналған ұпай:",
		"enemy_txt" : "Симптомдарды жою:",
		"miss_txt" : "Пропущено:",
		"time_txt" : "Қалған уақыт:",
		"restart_txt" : "Қайтадан көріңіз"
	},
	uz : {
		"score_txt" : "To’plangan ballar:",
		"enemy_txt" : "Alomatlar vayron qilingan:",
		"miss_txt" : " O'tkazib yuborilgan alomatlar:",
		"time_txt" : "Qolgan vaqt:",
		"restart_txt" : "Qayta urinib ko'ring"
	},
	md : {
		"score_txt" : "Puncte acumulate",
		"enemy_txt" : "Simptome distruse",
		"miss_txt"  : "Пропущено",
		"time_txt"  : "Timp rămas",
		"restart_txt" : "Încearcă din nou"
 	}
}

function getSiteLang() {
	let currentPathname = document.location.pathname;

	if ( currentPathname.includes('kz') ) {
		return 'kz';
	} else if ( currentPathname.includes('uz') ) {
		return 'uz';
	} else if ( currentPathname.includes('md') ) {
		return 'md';
	} else 
		return 'ru'
}