<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Леталка-Стрелялка</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<style>
	
</style>

<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
<script src="script.js"></script>

<link rel="stylesheet" href="styles.css">

<body>

<?php
require_once $_SERVER['DOCUMENT_ROOT']."/src/libs/Mobile-Detect/Mobile_Detect.php";
$detect = new Mobile_Detect;	
?>

	<section class="section-game">
		<div class="section__body">

		    <div class="game-block">
				<canvas id="game" width="1400" height="665">
				</canvas>				

				<div class="game__popup">
					<div class="game__btn" onclick="initGame();timer();$(this).closest('.game__popup').remove();">Начать игру</div>
				</div>

				<?php
				if ( $detect->isMobile() )  {
				?>
					<div class="game-block__mobile-controls">
						<button class="mobile-control mobile-control_left">
							<img src="src/img/icon-arrow.svg" alt="">
						</button>						
						<button class="mobile-control mobile-control_shoot">
							<img src="src/img/icon-shoot.svg" alt="">
						</button>
						<button class="mobile-control mobile-control_right">
							<img src="src/img/icon-arrow.svg" alt="">
						</button> 
					</div>
				<?php
			 	}
			 	?>
			</div>

			<div class="game-description-block">
				<p class="p_weight_500">
					Капитан, мы зарядили в пушки все 4 действия препарата для победы над вирусом:
				</p>
				<div class="bullets-description">
					<div class="bullet">
						<img src="bullet_icon_1.png" alt="" class="bullet__icon">
						<span class="bullet__name">Противовирусное</span>
					</div>
					<div class="bullet">
						<img src="bullet_icon_2.png" alt="" class="bullet__icon">
						<span class="bullet__name">Противовоспалительное</span>
					</div>
					<div class="bullet">
						<img src="bullet_icon_3.png" alt="" class="bullet__icon">
						<span class="bullet__name">Антигистаминное</span>
					</div>
					<div class="bullet">
						<img src="bullet_icon_4.png" alt="" class="bullet__icon">
						<span class="bullet__name">Иммуномодулирующее</span>
					</div>
				</div>

			</div>
		</div>
	</section>
</body>

</html>


